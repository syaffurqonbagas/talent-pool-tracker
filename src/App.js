import logo from './logo.svg';
import './App.css';


import { Sidebar } from './component/sidebar';
import { BrowserRouter } from 'react-router-dom';
import { Routers } from './routes/route';

function App() {
  return (
    <BrowserRouter>
      <Routers />
    </BrowserRouter>
  );
}

export default App;
