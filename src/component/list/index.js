import React from "react";
import { Button } from "react-bootstrap";
import user from "../../image/user.png"
import './style.css'

export const List = (props) => {
    return (
        <>
            <div className="list d-flex align-items-center px-lg-4 px-2 mt-5">
                <div className="avatar">
                    <img className="rounded" src={user}
                        width="55"
                    ></img>
                </div>
                <div className="title ms-3 mt-3 me-auto">
                    <h5 className="mb-1">Pt Sinar indo</h5>
                    <p>Company</p>
                </div>
                <div>
                    <Button className="me-lg-2">Update</Button>
                    <Button variant="danger" onClick={props.delete}>Delete</Button>
                </div>
            </div>
        </>
    )
}