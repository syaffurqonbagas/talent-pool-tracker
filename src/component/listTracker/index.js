import React from "react";
import { Button, Dropdown, DropdownButton } from "react-bootstrap";
import Badge from 'react-bootstrap/Badge'
import user from "../../image/user.png"

export const ListTracker = (props) => {
    return (
        <>
            <div className="list d-flex flex-column flex-lg-row align-items-center px-lg-4 px-2 mt-5">
                <div className="avatar">
                    <img className="rounded" src={user}
                        width="55"
                    ></img>
                </div>
                <div className="title ms-lg-3 mt-3 me-lg-auto text-center text-lg-start">
                    <h5 className="mb-1">Name</h5>
                    <p>Company</p>
                </div>
                <div className="me-lg-auto mb-3 mb-lg-0 bg-dark text-white d-flex align-items-center rounded">
                    <p className="mt-3 px-2 ">STATUS</p>
                </div>
                <div className="d-flex">
                    <DropdownButton className="me-2" id="dropdown-basic-button" title="Select Status">
                        <Dropdown.Item href="#/action-1">Review</Dropdown.Item>
                        <Dropdown.Item href="#/action-2">HR Interview</Dropdown.Item>
                        <Dropdown.Item href="#/action-3">User Interview</Dropdown.Item>
                        <Dropdown.Item href="#/action-3">Offer</Dropdown.Item>
                        <Dropdown.Item href="#/action-3">Accapted</Dropdown.Item>
                        <Dropdown.Item href="#/action-3">Rejected</Dropdown.Item>
                    </DropdownButton>
                    <Button variant="danger" onClick={props.delete}>Delete</Button>
                </div>
            </div>
        </>
    )
}