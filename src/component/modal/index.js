import React from "react";
import { Modal } from "react-bootstrap";
import { Button } from "react-bootstrap";

export const ModalConfirmation = (props) => {
    return (
        <Modal show={props.show} onHide={props.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Delete</Modal.Title>
            </Modal.Header>
            <Modal.Body>Are you sure ?</Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={props.handleClose}>
                    Close
                </Button>
                <Button variant="primary" onClick={props.handleClose}>
                    Delete
                </Button>
            </Modal.Footer>
        </Modal>
    )
}