import React from "react";
import "./style.css"
import { Container, NavDropdown, Nav, Navbar } from 'react-bootstrap';
import { Link } from "react-router-dom";
export const Sidebar = () => {
    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Link to="/" style={{ textDecoration: "none" }}> <Navbar.Brand>Pool-Tracker</Navbar.Brand></Link>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                        </Nav>
                        <Nav>
                            <Nav.Link href="/">Company</Nav.Link>

                            <Nav.Link href="/Pic" >
                                PIC
                            </Nav.Link>
                            <Nav.Link href="/Talent">Talent</Nav.Link>
                            <Nav.Link href="/Tracker">
                                Tracker
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )

}