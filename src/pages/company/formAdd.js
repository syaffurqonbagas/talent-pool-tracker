import React from "react";
import { Container } from "react-bootstrap";
import { FormPool } from "../../component/form";
import { Sidebar } from "../../component/sidebar";

export const FormAddCompany = () => {
    return (
        <>
            <Sidebar />
            <Container>
                <div className="my-5">
                    <h2>New Company</h2>
                </div>
                <div>
                    <FormPool />
                </div>
            </Container>
        </>
    )
}