import React from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { List } from "../../component/list";
import { useState } from "react";
import { ModalConfirmation } from "../../component/modal";

export const Company = () => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <div className="mt-4 d-flex">
                <h2 className="me-auto">Company</h2>
                <Link to="/AddCompany"><Button variant="outline-success" onK>Add</Button> </Link>
            </div>
            <ModalConfirmation show={show} handleClose={handleClose} />
            <List delete={handleShow}></List>
        </>
    )
}