import React from "react";
import { Container } from "react-bootstrap";
import { List } from "../component/list";
import { Sidebar } from "../component/sidebar";
import { Company } from "./company";

export const Homepage = () => {
    return (
        <>
            <Sidebar />
            <Container>
                <Company />
            </Container>
        </>

    )
}