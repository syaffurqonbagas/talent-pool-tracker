import React from "react";
import { Button, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { List } from "../../component/list";
import { Sidebar } from "../../component/sidebar";
import { useState } from "react";
import { ModalConfirmation } from "../../component/modal";

export const Talent = () => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <Sidebar />
            <Container>
                <div className="mt-4 d-flex">
                    <h2 className="me-auto">Talent</h2>
                    <Link to="/AddTalent"><Button variant="outline-success">Add</Button> </Link>
                </div>
                <ModalConfirmation show={show} handleClose={handleClose} />
                <List delete={handleShow}></List>
            </Container>

        </>
    )
}