import React from "react";
import { Container } from "react-bootstrap";
import { FormPool } from "../../component/form";
import { Sidebar } from "../../component/sidebar";

export const FormAddTracker = () => {
    return (
        <>
            <Sidebar />
            <Container>
                <div className="my-5">
                    <h2>New Tracker</h2>
                </div>
                <div>
                    <FormPool />
                </div>
            </Container>
        </>
    )
}