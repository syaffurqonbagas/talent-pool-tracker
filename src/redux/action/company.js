import { DELETE_DATA_COMPANY_BEGIN, GET_DATA_COMPANY_BEGIN, POST_DATA_COMPANY_BEGIN, PUT_DATA_COMPANY_BEGIN } from "./type"


export const getCompany = () => {
    return {
        type: GET_DATA_COMPANY_BEGIN,
    }
}

export const deleteCompany = (id) => {
    return {
        type: DELETE_DATA_COMPANY_BEGIN,
        id
    }
}

export const postCompany = (body) => {
    return {
        type: POST_DATA_COMPANY_BEGIN,
        body
    }
}

export const putCompany = (id, body) => {
    return {
        type: PUT_DATA_COMPANY_BEGIN,
        id,
        body
    }
}