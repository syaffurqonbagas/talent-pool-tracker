import { DELETE_DATA_PIC_BEGIN, GET_DATA_PIC_BEGIN, POST_DATA_PIC_BEGIN, PUT_DATA_PIC_BEGIN } from "./type"


export const getPic = () => {
    return {
        type: GET_DATA_PIC_BEGIN
    }
}

export const deletePic = (id) => {
    return {
        type: DELETE_DATA_PIC_BEGIN,
        id
    }
}

export const postPic = (body) => {
    return {
        type: POST_DATA_PIC_BEGIN,
        body
    }
}

export const putPic = (id, body) => {
    return {
        type: PUT_DATA_PIC_BEGIN,
        id,
        body
    }
}