import { DELETE_DATA_TALENT_BEGIN, GET_DATA_TALENT_BEGIN, POST_DATA_TALENT_BEGIN, PUT_DATA_TALENT_BEGIN } from "./type"

export const getTalent = () => {
    return {
        type: GET_DATA_TALENT_BEGIN
    }
}

export const deleteTalent = (id) => {
    return {
        type: DELETE_DATA_TALENT_BEGIN,
        id
    }
}

export const postTalent = (body) => {
    return {
        type: POST_DATA_TALENT_BEGIN,
        body
    }
}

export const putTalent = (id, body) => {
    return {
        type: PUT_DATA_TALENT_BEGIN,
        id,
        body
    }
}