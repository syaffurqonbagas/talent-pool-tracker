//GET
export const GET_DATA_COMPANY_BEGIN = "GET_DATA_COMPANY_BEGIN"
export const GET_DATA_COMPANY_SUCCESS = "GET_DATA_COMPANY_SUCCESS"
export const GET_DATA_COMPANY_FAILED = "GET_DATA_COMPANY_FAILED"

export const GET_DATA_PIC_BEGIN = "GET_DATA_PIC_BEGIN"
export const GET_DATA_PIC_SUCCESS = "GET_DATA_PIC_SUCCESS"
export const GET_DATA_PIC_FAILED = "GET_DATA_PIC_FAILED"

export const GET_DATA_TALENT_BEGIN = "GET_DATA_TALENT_BEGIN"
export const GET_DATA_TALENT_SUCCESS = "GET_DATA_TALENT_SUCCESS"
export const GET_DATA_TALENT_FAILED = "GET_DATA_TALENT_FAILED"
//-------------------------------------------------------------
//-------------------------------------------------------------
//POST
export const POST_DATA_COMPANY_BEGIN = "POST_DATA_COMPANY_BEGIN"
export const POST_DATA_COMPANY_SUCCESS = "POST_DATA_COMPANY_SUCCESS"
export const POST_DATA_COMPANY_FAILED = "POST_DATA_COMPANY_FAILED"

export const POST_DATA_PIC_BEGIN = "POST_DATA_PIC_BEGIN"
export const POST_DATA_PIC_SUCCESS = "POST_DATA_PIC_SUCCESS"
export const POST_DATA_PIC_FAILED = "POST_DATA_PIC_FAILED"

export const POST_DATA_TALENT_BEGIN = "POST_DATA_TALENT_BEGIN"
export const POST_DATA_TALENT_SUCCESS = "POST_DATA_TALENT_SUCCESS"
export const POST_DATA_TALENT_FAILED = "POST_DATA_TALENT_FAILED"
//-------------------------------------------------------------
//-------------------------------------------------------------
//DELETE
export const DELETE_DATA_COMPANY_BEGIN = "DELETE_DATA_COMPANY_BEGIN"
export const DELETE_DATA_COMPANY_SUCCESS = "DELETE_DATA_COMPANY_SUCCESS"
export const DELETE_DATA_COMPANY_FAILED = "DELETE_DATA_COMPANY_FAILED"

export const DELETE_DATA_PIC_BEGIN = "DELETE_DATA_PIC_BEGIN"
export const DELETE_DATA_PIC_SUCCESS = "DELETE_DATA_PIC_SUCCESS"
export const DELETE_DATA_PIC_FAILED = "DELETE_DATA_PIC_FAILED"

export const DELETE_DATA_TALENT_BEGIN = "DELETE_DATA_TALENT_BEGIN"
export const DELETE_DATA_TALENT_SUCCESS = "DELETE_DATA_TALENT_SUCCESS"
export const DELETE_DATA_TALENT_FAILED = "DELETE_DATA_TALENT_FAILED"
//-------------------------------------------------------------
//-------------------------------------------------------------
//PUT
export const PUT_DATA_COMPANY_BEGIN = "PUT_DATA_COMPANY_BEGIN"
export const PUT_DATA_COMPANY_SUCCESS = "PUT_DATA_COMPANY_SUCCESS"
export const PUT_DATA_COMPANY_FAILED = "PUT_DATA_COMPANY_FAILED"

export const PUT_DATA_PIC_BEGIN = "PUT_DATA_PIC_BEGIN"
export const PUT_DATA_PIC_SUCCESS = "PUT_DATA_PIC_SUCCESS"
export const PUT_DATA_PIC_FAILED = "PUT_DATA_PIC_FAILED"

export const PUT_DATA_TALENT_BEGIN = "PUT_DATA_TALENT_BEGIN"
export const PUT_DATA_TALENT_SUCCESS = "PUT_DATA_TALENT_SUCCESS"
export const PUT_DATA_TALENT_FAILED = "PUT_DATA_TALENT_FAILED"
//-------------------------------------------------------------
//-------------------------------------------------------------