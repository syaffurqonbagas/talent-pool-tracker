import { combineReducers } from "redux";
import company from "./company";
import pic from "./pic";
import talent from "./talent";

export default combineReducers({
    company,
    pic,
    talent
})