import { DELETE_DATA_PIC_BEGIN, DELETE_DATA_PIC_FAILED, DELETE_DATA_PIC_SUCCESS, GET_DATA_PIC_BEGIN, GET_DATA_PIC_FAILED, GET_DATA_PIC_SUCCESS, POST_DATA_COMPANY_BEGIN, POST_DATA_COMPANY_FAILED, POST_DATA_COMPANY_SUCCESS, POST_DATA_PIC_BEGIN, PUT_DATA_COMPANY_BEGIN, PUT_DATA_COMPANY_FAILED, PUT_DATA_COMPANY_SUCCESS } from "../action/type"


const intialState = {
    listPic: [],
    loading: false,
    error: null
}

const pic = (state = intialState, action) => {
    const { type, payload, error } = action

    switch (type) {
        case GET_DATA_PIC_BEGIN:
            return {
                ...state,
                loading: true
            }
        case GET_DATA_PIC_SUCCESS:
            return {
                ...state,
                listPic: payload,
                loading: false,
                error: null
            }
        case GET_DATA_PIC_FAILED:
            return {
                ...state,
                loading: false,
                error: error
            }
        case DELETE_DATA_PIC_BEGIN:
            return {
                ...state,
                loading: true
            }
        case DELETE_DATA_PIC_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null
            }
        case DELETE_DATA_PIC_FAILED:
            return {
                ...state,
                loading: false,
                error: null
            }
        case POST_DATA_PIC_BEGIN:
            return {
                ...state,
                loading: true
            }
        case POST_DATA_COMPANY_SUCCESS:
            return {
                ...state,
                listPic: [...payload, ...state.listPic],
                loading: false,
                error: null
            }
        case POST_DATA_COMPANY_FAILED:
            return {
                ...state,
                loading: false,
                error: error
            }
        case PUT_DATA_COMPANY_BEGIN:
            return {
                ...state,
                loading: true
            }
        case PUT_DATA_COMPANY_SUCCESS:
            return {
                ...state,
                listPic: payload,
                loading: false,
                error: null
            }
        case PUT_DATA_COMPANY_FAILED:
            return {
                ...state,
                loading: false,
                error: error
            }
        default:
            return {
                ...state
            }
    }
}

export default pic;