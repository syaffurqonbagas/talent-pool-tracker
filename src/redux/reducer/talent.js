import { DELETE_DATA_TALENT_BEGIN, DELETE_DATA_TALENT_FAILED, DELETE_DATA_TALENT_SUCCESS, GET_DATA_TALENT_BEGIN, GET_DATA_TALENT_FAILED, GET_DATA_TALENT_SUCCESS, POST_DATA_TALENT_BEGIN, POST_DATA_TALENT_FAILED, POST_DATA_TALENT_SUCCESS, PUT_DATA_TALENT_BEGIN, PUT_DATA_TALENT_FAILED, PUT_DATA_TALENT_SUCCESS } from "../action/type"


const intialState = {
    listTalent: [],
    loading: false,
    error: null
}

const talent = (state = intialState, action) => {
    const { type, payload, error } = action

    switch (type) {
        case GET_DATA_TALENT_BEGIN:
            return {
                ...state,
                loading: true
            }
        case GET_DATA_TALENT_SUCCESS:
            return {
                ...state,
                listTalent: payload,
                loading: false,
                error: null
            }
        case GET_DATA_TALENT_FAILED:
            return {
                ...state,
                loading: false,
                error: error
            }
        case DELETE_DATA_TALENT_BEGIN:
            return {
                ...state,
                loading: true
            }
        case DELETE_DATA_TALENT_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null
            }
        case DELETE_DATA_TALENT_FAILED:
            return {
                ...state,
                loading: false,
                error: error
            }
        case POST_DATA_TALENT_BEGIN:
            return {
                ...state,
                loading: true
            }
        case POST_DATA_TALENT_SUCCESS:
            return {
                ...state,
                listTalent: [...payload, ...state.listTalent],
                loading: false,
                error: null
            }
        case POST_DATA_TALENT_FAILED:
            return {
                ...state,
                loading: false,
                error: error
            }
        case PUT_DATA_TALENT_BEGIN:
            return {
                ...state,
                loading: true
            }
        case PUT_DATA_TALENT_SUCCESS:
            return {
                ...state,
                listTalent: payload,
                loading: false,
                error: null
            }
        case PUT_DATA_TALENT_FAILED:
            return {
                ...state,
                loading: false,
                error: error
            }
        default:
            return {
                ...state
            }
    }
}

export default talent;