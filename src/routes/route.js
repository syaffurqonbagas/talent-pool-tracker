import React from "react";
import { Route } from "react-router";
import { Switch } from "react-router-dom/cjs/react-router-dom.min";
import { FormAddCompany } from "../pages/company/formAdd";
import { Homepage } from "../pages/homepage";
import { Pic } from "../pages/pic";
import { FormAddPIC } from "../pages/pic/formAdd";
import { Talent } from "../pages/talent";
import { FormAddTalent } from "../pages/talent/formAdd";
import { Tracker } from "../pages/tracker";
import { FormAddTracker } from "../pages/tracker/formAdd";

export const Routers = () => {
    return (
        <Switch>
            <Route exact path="/">
                <Homepage />
            </Route>
            <Route exact path="/Pic">
                <Pic />
            </Route>
            <Route exact path="/Talent">
                <Talent />
            </Route>
            <Route exact path="/Tracker">
                <Tracker />
            </Route>
            <Route exact path="/AddCompany">
                <FormAddCompany />
            </Route>
            <Route exact path="/AddPIC">
                <FormAddPIC />
            </Route>
            <Route exact path="/AddTalent">
                <FormAddTalent />
            </Route>
            <Route exact path="/AddTracker">
                <FormAddTracker />
            </Route>
        </Switch>
    )
}